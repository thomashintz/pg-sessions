(asdf:defsystem #:pg-sessions
  :serial t
  :description "Store hunchentoot sessions in a postgres database."
  :author "Thomas Hintz"
  :license "4-clause BSD."
  :depends-on (#:postmodern
               #:anaphora
               #:alexandria
               #:cl-ppcre
               #:hunchentoot)
  :components ((:file "package")
               (:file "pg-sessions")))

